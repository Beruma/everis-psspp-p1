package dam1.psspp.p1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Actividad7 {
    public void crearProceso(){
        try{
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("java", "dam1.psspp.p1.MostrarMayusculas");
            processBuilder.directory(new File("./out/production/ejercicio7tema1/dam1/psspp/p1"));
            Process process = processBuilder.start();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while((line = bufferedReader.readLine())!= null){
                System.out.println(line);
            }
            int codigo = process.waitFor();
            System.out.println("finalizando con código de error: " + codigo);
        }catch (IOException e){
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Actividad7 actividad7 = new Actividad7();
        actividad7.crearProceso();
    }
}
