package dam1.psspp.p1;

import java.util.Scanner;

/*
 * Realiza un programa Java llamado MostrarMayusculas que reciba desde línea de comandos un única parámetro tipo texto
 * y lo muestre por pantalla en mayúsculas.
 * Utiliza códigos de error para los casos en que el programa finalice de forma incorrecta.
 * A continuación, crea otro programa Java llamado Actividad7, que cree un subproceso correspondiente al programa
 * anterior.
 * Este programa (proceso padre) deberá comprobar los códigos de error generados por la salida del proceso hijo y
 * mostrarlos por pantalla.
 */
public class MostrarMayusculas {
    public static void main(String[] args) {
        Scanner key = new Scanner(System.in);
        System.out.println("introduzca una palabra");
        String palabra = key.nextLine();
        System.out.println(palabra.toUpperCase());
    }
}
